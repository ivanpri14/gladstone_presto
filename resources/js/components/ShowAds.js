import React, { useState } from 'react';

export default function ShowAds(props) {
    let [i, setI] = useState(0);
    let [end, setEnd] = useState(10);
    const allAds = [];
    const backI = i - 10;
    const backEnd = end - 10;
    const followingI = i + 10;
    const followingEnd = end + 10;
    const backDisabled = i == 0;
    const followingDisabled = end > props.ads.length;
    const pages = Math.ceil(props.ads.length / 10);
    const pagesArray = [];
    const categoryAll = [];

    if (props.categories) {
        props.categories.forEach(element => {
            categoryAll.push(
                {
                    id: element.id,
                    name: element.name
                });
        });
    }

    const category = categoryAll.map((category) => {
        return (
            <div key={category.id} className="col-6 col-md-4 col-lg-3">
                <div className="my-3 card-category rounded">
                    <div className="card-body font-weight-bold">
                        <button className="btn" onClick={() => props.selectCategory(category.name)}>{category.name}</button>
                    </div>
                </div>
            </div>
        )
    })

    if (props.ads) {
        for (i; i < props.ads.length && i < end; i++) {
            allAds.push(
                {
                    id: props.ads[i].id,
                    title: props.ads[i].title,
                    price: props.ads[i].price,
                    description: props.ads[i].description,
                    user: props.ads[i].user.name,
                    category: props.ads[i].category.name,
                    image: props.ads[i].images[0] ? props.ads[i].images[0].img : "",
                }
            );
        }
    }

    const ad = allAds.map((ad) => {
        if (ad.category != props.selectedCategory && props.selectedCategory) {
            return;
        } else {
            return (
                <div key={ad.id} className="card mb-3" >
                    <div className="row no-gutters">
                        <div className="col-md-4">
                            <img src={ad.image} className="card-img" alt="..." />
                        </div>
                        <div className="col-md-8">
                            <div className="card-body">
                                <h5 className="card-title">{ad.title}</h5>
                                <p className="card-text">{ad.description}</p>
                                <p className="card-text">{ad.category}</p>
                                <p className="card-text d-flex justify-content-between"><small>Creato da: {ad.user}</small> <span>{ad.price}€</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    })

    if (pages) {
        for (let p = 0; p < pages; p++) {
            pagesArray.push(p + 1);
        }
    }

    const numberPage = pagesArray.map((number, index) => {
        return (
            <li key={index} className="page-item"><button className="page-link" onClick={() => { setI(index * 10); setEnd(number * 10) }}>{number}</button></li>
        )
    })

    return (
        <>
            <div className="container rounded">
                <div className="row text-center">{category}</div>
            </div>
            <div className="container">
                <div className="row mt-5" >
                    <div className="col-12" >{ad}</div>
                </div>
                <div className="row mt-5" >
                    <div className="col-5" >
                        <nav aria-label="Page navigation ads">
                            <ul className="pagination">
                                <li className="page-item"><button className="page-link" onClick={() => { setI(backI); setEnd(backEnd) }} disabled={backDisabled}>Previous</button></li>
                                {numberPage}
                                <li className="page-item"><button className="page-link" onClick={() => { setI(followingI); setEnd(followingEnd) }} disabled={followingDisabled}>Next</button></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
};