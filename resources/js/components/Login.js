import React from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useHistory } from 'react-router-dom';

export default function Login() {
  const { register, handleSubmit } = useForm();
  const history = useHistory();

  const onSubmit = data => axios.get('/sanctum/csrf-cookie').then(response => {
    axios({
      method: 'post',
      url: '/login/authenticate',
      data: data,
    })
      .then(function (response) { history.push('/'); history.go(0) })
      .catch(function (error) {
        console.log(error);
      });
  });

  return (
    <form className='container my-5' onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input type="email" name="email" className="form-control" id="email" ref={register} />
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input type="password" name="password" className="form-control" id="password" ref={register} />
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  )
};