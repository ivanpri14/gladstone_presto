import React from 'react';
import { Link } from "react-router-dom";

export default function Home(props) {
    const lastAds = [];
    const categoryAll = [];

    if (props.ads) {
        for (let i = 0; i < props.ads.length && i < 5; i++) {
            lastAds.push(
                {
                    id: props.ads[i].id,
                    title: props.ads[i].title,
                    price: props.ads[i].price,
                    description: props.ads[i].description,
                    user: props.ads[i].user.name,
                    category: props.ads[i].category.name,
                    image: props.ads[i].images[0] ? props.ads[i].images[0].img : "",
                }
            );
        }
    }

    const ad = lastAds.map((ad) => {
        return (
            <div key={ad.id} className="card mb-3" >
                <div className="row no-gutters">
                    <div className="col-md-4">
                        <img src={ad.image} className="card-img" alt="..." />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <h5 className="card-title">{ad.title}</h5>
                            <p className="card-text">{ad.description}</p>
                            <p className="card-text">{ad.category}</p>
                            <p className="card-text d-flex justify-content-between"><small>Creato da: {ad.user}</small> <span>{ad.price}€</span></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    })

    if (props.categories) {
        props.categories.forEach(element => {
            categoryAll.push(
                {
                    id: element.id,
                    name: element.name
                });
        });
    }



    const category = categoryAll.map((category) => {
        return (
            <div key={category.id} className="col-6 col-md-4 col-lg-3">
                <div className="my-3 card-category rounded">
                    <div className="card-body font-weight-bold">
                        <Link to='/ShowAds' className="btn" onClick={() => props.selectCategory(category.name)}>{category.name}</Link>
                    </div>
                </div>
            </div>
        )
    })

    return (
        <div>
            <div className="container-fluid">
                <div className="row bg-handshake">
                    <div className="col-12 mt-4">
                        <h1>Gladstone</h1>
                        <h2>Vendi e compra, con un click</h2>
                    </div>
                </div>
            </div>
            <div className="container rounded">
                <div className="row text-center">{category}</div>
            </div>
            <div className="container">
                <div className="row mt-5" >
                    <div className="col-12" >{ad}</div>
                </div>
            </div>
        </div>
    )
};