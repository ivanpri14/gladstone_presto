import React, { useState } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useHistory } from 'react-router-dom';

export default function CreateAd(categories) {
    const { register, handleSubmit, errors, reset, watch } = useForm();
    const history = useHistory();
    const watchImg = watch("img", false);
    const [files, setFiles] = useState([]);
    const categoryAll = [];

    if (categories.categories) {
        categories.categories.forEach(element => {
            categoryAll.push(
                {
                    id: element.id,
                    name: element.name
                });
        });
    }

    const optionCategory = categoryAll.map((category) => {
        return <option key={category.id} value={category.id} >{category.name}</option>
    });

    const preview = () => {
        setFiles([...watchImg].map(file => Object.assign(file, {
            preview: URL.createObjectURL(file)
        })));
    };

    const thumbs = files.map(file => (
        <div className="thumb" key={file.name}>
            <div className="thumbInner">
                <img
                    src={file.preview}
                    className="preview-img"
                />
            </div>
        </div>
    ));

    const onSubmit = data => {
        const formData = new FormData();
        for (let i = 0; i < data.img.length; i++) {
            formData.append('img' + [i], data.img[i]);
        }
        formData.append('title', data.title);
        formData.append('category_id', data.category_id);
        formData.append('price', data.price);
        formData.append('description', data.description);
        axios({
            method: 'post',
            url: '/ad/store',
            data: formData,
            headers: {
                'content-type': 'multipart/form-data'
            }
        })
            .then(function (response) { alert(response.data); reset(response); })
            .catch(function (response) {
                console.log(response);
            });
    };

    return (
        categories.categories &&
        <form className='container my-5' onSubmit={handleSubmit(onSubmit)}>
            <div className='form-row'>
                <div className="form-group col-12">
                    <label htmlFor="title">Titolo</label>
                    <input id='title' type='text' className='form-control' name="title" ref={register({ required: "E' richiesto il titolo", maxLength: { value: 50, message: 'Hai scritto troppo' } })} />
                    {errors.title && <div className='alert alert-danger mt-2' >{errors.title?.message}</div>}
                </div>
            </div>
            <div className='form-row'>
                <div className="form-group col-md-6">
                    <label htmlFor="category">Categoria</label>
                    <select className="form-control" id="category_id" ref={register({ required: "E' richiesta una categoria" })} name='category_id'>
                        <option value=''> Scegli una categoria</option>
                        {optionCategory}
                    </select>
                    {errors.category_id && <div className='alert alert-danger mt-2' >{errors.category_id?.message}</div>}
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="price">Prezzo</label>
                    <input type='number' step=".01" className='form-control' name="price" id='price' ref={register({ required: "E' richiesto il prezzo" })} />
                    {errors.price && <div className='alert alert-danger mt-2' >{errors.price?.message}</div>}
                </div>
            </div>
            <div className='form-row'>
                <div className="form-group col-12">
                    <label htmlFor="description">Descrizione</label>
                    <textarea className='form-control' rows='5' name="description" ref={register({ required: "E' richiesta la descrizione", maxLength: { value: 500, message: 'Hai scritto troppo' }, minLength: { value: 10, message: 'Hai scritto troppo poco' } })}></textarea>
                </div>
                {errors.description && <div className='alert alert-danger mt-2' >{errors.description?.message}</div>}
            </div>
            <div className='input-group'>
                <div className="custom-file">
                    <label htmlFor="img" className='custom-file-label'>Inserisci le tue immagini, massimo 5</label>
                    <input id='img' type='file' className='custom-file-input' name="img[]" multiple ref={register} aria-describedby="img" onChange={preview} />
                </div>
            </div>
            <aside className="thumbsContainer">
                {thumbs}
            </aside>
            <input type="submit" />
        </form>
    );
}