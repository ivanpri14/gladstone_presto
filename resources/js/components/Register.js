import React from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import { useHistory } from 'react-router-dom';

export default function Register() {
    const { register, handleSubmit } = useForm();
    const history = useHistory();

    //const onSubmit = data => console.log(data);
    const onSubmit = async (data) => {
        //const bodyData = new FormData(data);
        await axios({
            method: 'post',
            url: '/register/store',
            data: data,
        })
            .then(function (response) { history.push('/'); history.go(0) })
            .catch(function (response) {
                console.log(response);
            });
    };

    return (
        <form className='container my-5' onSubmit={handleSubmit(onSubmit)}>
            <div className='form-row'>
                <div className="form-group col-md-6">
                    <label htmlFor="firstName">Nome</label>
                    <input id='firstName' type='text' className='form-control' name="firstName" ref={register} />
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="lastName">Cognome</label>
                    <input type='text' className='form-control' name="lastName" ref={register} />
                </div>
            </div>
            <div className='form-row'>
                <div className="form-group col-md-6">
                    <label htmlFor="email">E-mail</label>
                    <input type='email' className='form-control' name="email" ref={register} />
                </div>
                <div className="form-group col-md-6">
                    <label htmlFor="password">Password</label>
                    <input type='password' className='form-control' name="password" ref={register} />
                </div>
            </div>
            <input type="submit" />
        </form>
    );
}