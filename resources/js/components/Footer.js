import React from 'react';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import {
    faFacebookF,
    faTwitter,
    faInstagram
} from '@fortawesome/free-brands-svg-icons';

export default function Footer() {

    return (
        <footer className="mt-5">
            <div className="row mx-auto bg-footer text-white pt-5">
                {/* Prima colonna  */}
                <div className="col-12 col-md-6 col-xl-4">
                    <div className="col-12 col-sm-12 p-5">
                        <Link to="/" className="d-flex align-items-center">
                            <img src="/media/quadrifoglio.png" width="50px" alt="Logo" />
                            <div id="logo" className="h1 d-inline text-white ml-2">Gladstone</div>
                        </Link>
                        <div className="col-12 px-0">
                            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempora nisi laudantium natus debitis reiciendis.</p>
                        </div>
                    </div>
                </div>
                {/* Seconda colonna */}
                <div className="col-12 col-md-6 col-xl-4 py-5 mb-5 py-md-0 mb-md-0 align-self-center">
                    <div className="d-flex d-inline justify-content-center text-center">
                        <div className="mx-4 font-weight-bold"><a className="link-footer text-white" href="">Link</a></div>
                        <div className="mx-4 font-weight-bold"><a className="link-footer text-white" href="">Link</a></div>
                        <div className="mx-4 font-weight-bold"><a className="link-footer text-white" href="">Link</a></div>
                        <div className="mx-4 font-weight-bold"><a className="link-footer text-white" href="">Link</a></div>
                    </div>
                </div>
                {/* Terza colonna */}
                <div className="col-12 col-xl-4 my-5 align-self-center">
                    <h5 className="text-center mt-n5 mb-4">Social</h5>
                    <div className="d-flex justify-content-center">
                        <div className="social-icons">
                            <a className="social-icon mx-1 mx-sm-2" target="_blank" href="#"><FontAwesomeIcon icon={faFacebookF} /></a>
                            <a className="social-icon mx-1 mx-sm-2" target="_blank" href="#"><FontAwesomeIcon icon={faTwitter} /></a>
                            <a className="social-icon mx-1 mx-sm-2" target="_blank" href="#"><FontAwesomeIcon icon={faInstagram} /></a>
                            <a className="social-icon mx-1 mx-sm-2" href="#"><FontAwesomeIcon icon={faEnvelope} /></a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container-fluid py-3 bg-copyright">
                <div className="small text-center text-white">
                    Copyright © 2020 - Gladstone
                </div>
            </div>
        </footer>
    )
};