import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import axios from 'axios';
import Navbar from './Navbar';
import Home from './Home';
import Register from './Register';
import Login from './Login';
import CreateAd from './CreateAd';
import Footer from './Footer';
import ShowAds from './ShowAds';


export default function App() {
    const [logged, setLogged] = useState(false);
    const [categories, setCategories] = useState(false);
    const [ads, setAds] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState('');

    useEffect(() => {
        if (!logged) {
            function getUser() {
                axios.get('/loggedIn').then((response) => { setLogged(response.data); })
                    .catch((error) => {
                        console.error(error)
                    });
            }
            getUser();
        };
    }, []);

    if (!categories) {
        function getCategory() {
            return axios.get('/categories');
        };
        function getAds() {
            return axios.get('/get/allAds');
        }

        Promise.all([getCategory(), getAds()])
            .then(function (results) {
                setCategories(results[0].data);
                setAds(results[1].data);
            });
    };

    const selectCategory = (category) => {
        setSelectedCategory(category)
    }

    return (
        <Router>
            <Navbar logged={logged} selectCategory={selectCategory} />
            <Switch>
                <Route path='/register' component={Register} />
                <Route path='/login' component={Login} />
                <Route path='/createAd'>
                    <CreateAd categories={categories} />
                </Route>
                <Route path='/ShowAds'>
                    <ShowAds ads={ads} selectedCategory={selectedCategory} categories={categories} selectCategory={selectCategory} />
                </Route>
                <Route path='/'>
                    <Home ads={ads} categories={categories} selectCategory={selectCategory} />
                </Route>
            </Switch>
            <Footer />
        </Router>
    );
}

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}