import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import { useHistory } from 'react-router-dom';

export default function Navbar(props) {
    const loggedIn = props.logged;
    const history = useHistory();

    const logout = () => axios.get('/logout')
        .then(function (response) {
            history.push('/'); history.go(0);
        })
        .catch(function (error) {
            console.log(error);
        });

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-accent">
            <a className="navbar-brand" href="#">
                <img src='./media/quadrifoglio.png' className='rounded' width='50' />
            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link className="btn btn-outline-light mr-lg-2" to='/'>Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="btn btn-outline-light mr-lg-2" to='/ShowAds' onClick={() => props.selectCategory('')}>Guarda tutti gli annunci</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="btn btn-outline-light" to='/createAd'>Crea un annuncio</Link>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto mr-lg-4">
                    {loggedIn == false && (
                        <>
                            <li className="nav-item ">
                                <Link className="btn btn-outline-light mr-lg-2" to='register'>Registrati</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="btn btn-outline-light" to='login'>Login</Link>
                            </li>
                        </>
                    )}
                    {loggedIn != false && (
                        <>
                            <li className="nav-item mr-2">
                                <img src='https://picsum.photos/40' />
                            </li>
                            <li className="nav-item dropdown">
                                <button className=" btn nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{loggedIn.name}</button>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <button className="btn dropdown-item" onClick={() => logout()}>Logout</button>
                                </div>
                            </li>
                        </>
                    )}
                </ul>
            </div>
        </nav>
    );
}