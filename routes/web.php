<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/register/store' , [RegisterController::class, 'store']);
Route::post('/ad/store' , [AdController::class, 'storeAd']);
Route::post('/login/authenticate' , [LoginController::class, 'authenticate']);
Route::get('/logout', [LoginController::class, 'logout']);


Route::get('/get/allAds', [PublicController::class, 'allAds']);
Route::get('/loggedIn', [PublicController::class, 'loggedIn']);
Route::get('/categories', [PublicController::class, 'categories']);
Route::get('/{any?}', [PublicController::class, 'home'])->name('home');
//Route::get('/{any}', [PublicController::class, 'home'])->where('any', '.*');
