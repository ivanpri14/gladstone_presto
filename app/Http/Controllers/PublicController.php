<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function home(){
        return view('welcome');
    }

    public function loggedIn(){

        if (Auth::user()) {
            $user = Auth::user();
        } else {
            $user = false;
        }
        return response()->json($user);

    }

    public function categories(){
        $categories = Category::all();
        return response()->json($categories);
    }

    public function allAds(){
        $ads = Ad::orderByDesc('id')->get()->load(['user', 'category', 'images']);
        return response()->json($ads);
    }
}
