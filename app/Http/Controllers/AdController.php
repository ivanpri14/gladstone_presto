<?php

namespace App\Http\Controllers;

use App\Models\AdImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdController extends Controller
{
    public function storeAd(Request $request){

        if (Auth::user()){
            $user=Auth::user();
            $ad = $user->ads()->create(
                [
                    'title'=> $request->title,
                    'description'=>$request->description,
                    'price'=>$request->price,
                    'category_id'=>$request->category_id
                    ]
                );

            $images = [
                $request->file('img0'), $request->file('img1'),
                $request->file('img2'), $request->file('img3'), $request->file('img4')
            ];

            foreach ($images as $image) {
                if ($image != null) {
                    $img = $image->store("public/ad/{$ad->id}");
                        $i = new AdImage();

                        $fileName = basename($img);
                        $newFileName = "storage/ad/{$ad->id}/{$fileName}";

                        $i->img = $newFileName;
                        $i->ad_id = $ad->id;

                        $i->save();
                    }
                };

            return response()->json('inserito annuncio');
        } 
        else {
            return response()->json('non sei autenticato');
        }

    }

}
