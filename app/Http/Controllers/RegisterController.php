<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    use RegistersUsers;

    public function store(Request $request){
        
        /*$user = new User;
        $user->name = $request->input('firstName');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));

        $user->save();*/

        $user = User::create(
            [
                'name' => $request->input('firstName') . ' ' . $request->input('lastName'), 
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
            ]
        );

        Auth::login($user);
        return response()->json('ok');
    }
}
